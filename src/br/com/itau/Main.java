package br.com.itau;

import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {

        Pessoa pessoa = new Pessoa("Fernando", 34);
        Funcionario funcionario = new Funcionario(4287454);

        System.out.println(pessoa.getIdadePessoa());
        System.out.println(funcionario.getIdadePessoa());
    }
}
