package br.com.itau;

public class Funcionario extends Pessoa{
    public int racf;

    public Funcionario(String nome, int idade, int racf) {
        super(nome, idade);
        this.racf = racf;
    }

    public Funcionario(int racf) {
        this.racf = racf;
    }

    public Funcionario(){}

    public int getRacf() {
        return racf;
    }

    public void setRacf(int racf) {
        this.racf = racf;
    }

    @Override
    public int getIdadePessoa(){
        return racf;
    }
}
